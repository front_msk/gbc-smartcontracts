const token = artifacts.require("WinWinCoinToken")
const whitelist = artifacts.require("WinWinCoinWhitelist")
const crowdsale = artifacts.require("WinWinCoinCrowdsale")

module.exports = (deployer) => {
    var wallet = "0x5431444a48aa118b6cd00ab7ccb5a1b2fac3f264"

    return deployer.then(async () => {
        await deployer.deploy(token)
        await deployer.deploy(whitelist)
        await deployer.deploy(
            crowdsale,
            wallet,
            token.address,
            whitelist.address
        )        
        const tokenContract = await token.deployed()
        await tokenContract.freeze()
        await tokenContract.transferOwnership(crowdsale.address)
    })
}