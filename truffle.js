const HDWalletProvider = require("truffle-hdwallet-provider");
require('dotenv').config();

const mnemonic = "*";
const key = "*";

module.exports = {    
    networks: {
        development: {
            host: "localhost",
            port: 7545,
            network_id: "*",
        },
        kovan: {
            provider: () => new HDWalletProvider(mnemonic, `https://kovan.infura.io/v3/${key}`),
            network_id: 3,
            gas: 5000000,
            gasPrice: 60000000000
        },
        ropsten: {
            provider: () => new HDWalletProvider(mnemonic, `https://ropsten.infura.io/v3/${key}`),
            network_id: 3,
            gas: 5000000,
            gasPrice: 60000000000
        }
    }
};