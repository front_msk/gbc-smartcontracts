const WinWinCoinCrowdsale = artifacts.require('WinWinCoinCrowdsale');
const WinWinCoinToken = artifacts.require("WinWinCoinToken");
const WinWinCoinWhitelist = artifacts.require("WinWinCoinWhitelist");

contract('WinWinCoinCrowdsaleTest', (accounts) => {
    const account1 = accounts[0];
    const account2 = accounts[1];

    const tokensLimit = 200 * 1000 * 1000;
    const privateSalePrice = web3.toWei("0.00083", "ether");
    const preSalePrice = web3.toWei("0.00167", "ether");
    const sale1Price = web3.toWei("0.00250", "ether");
    const sale2Price = web3.toWei("0.00333", "ether");

    const airdropPrice = web3.toWei("0.00333", "ether");

    const privateSaleMinCount = 4000;
    const preSaleMinCount = 20;
    const sale1SaleMinCount = 15;
    const sale2SaleMinCount = 10;

    context('buyout', async () => {
        beforeEach(async () => {
            this.token = await WinWinCoinToken.new();
            this.whitelist = await WinWinCoinWhitelist.new();
            this.crowdsale = await WinWinCoinCrowdsale.new(account1, this.token.address, this.whitelist.address);
            await this.token.transferOwnership(this.crowdsale.address);    
            await this.whitelist.addAddressToWhitelist(account1);
        });

        it('should check buyout fund 1', async () => {            
            await this.crowdsale.buyTokens(account1, 0x0, { from: account1, value: 5000 * privateSalePrice });
            await this.crowdsale.buyTokens(account2, 0x0, { from: account2, value: 4000 * privateSalePrice });

            await this.crowdsale.enableAirdrop({ from: account1 });
            await this.crowdsale.buyWithAirdrop(account2, { from: account2, value: 20 * airdropPrice });

            const boFund = await this.crowdsale.buyoutFund();
            
            assert.equal(boFund.valueOf(), (privateSalePrice * 9000 + airdropPrice * 20)  * 0.3 );
        });

        it('should check buyout transfer', async () => {            
            await this.crowdsale.buyTokens(account2, 0x0, { from: account2, value: 6000 * privateSalePrice });
            await this.crowdsale.setPresaleStage({ from: account1 });
            
            await this.crowdsale.buyout(890, { from: account2 });

            const tokensCount =  await this.token.balanceOf(account2);  
            const tokensCountOwner =  await this.token.balanceOf(this.crowdsale.address);  
            
            assert.equal(tokensCount.valueOf(), 5110);
            assert.equal(tokensCountOwner.valueOf(), tokensLimit - 5110);
        }); 

        it('should check buyout fund 2', async () => {            
            await this.crowdsale.buyTokens(account2, 0x0, { from: account2, value: 6000 * privateSalePrice });
            await this.crowdsale.setPresaleStage({ from: account1 });

            const fundBefore = await this.crowdsale.buyoutFund();            
            const balanceBefore = await web3.eth.getBalance(this.crowdsale.address);

            assert.equal(fundBefore.valueOf(), balanceBefore.valueOf());
            assert.equal(fundBefore.valueOf(), 6000 * 0.3 * privateSalePrice);

            await this.crowdsale.buyout(500, { from: account2 });

            const fundAfter = await this.crowdsale.buyoutFund();
            const balanceAfter = await web3.eth.getBalance(this.crowdsale.address);

            assert.equal(fundAfter.valueOf(), balanceAfter.valueOf());
            
            const diff = balanceBefore.valueOf() - balanceAfter.valueOf();

            assert.equal(diff, 500 * preSalePrice);

            try {
                await this.crowdsale.buyout(500, { from: account2 });
            } catch (error) {                
                const isRevert = error.message.search('Not enougth eth in buyout fund') >= 0;                
                assert(
                   isRevert,
                  "Expected throw, got '" + error + "' instead",
                );                
            }            
        });

        it('should check buyout limit on sale 1', async () => {            
            // add 0.00083 * 6000 * 0.3 = 1.494ETH to buyout fund
            await this.crowdsale.buyTokens(account1, 0x0, { from: account1, value: 6000 * privateSalePrice });
            
            await this.crowdsale.setPresaleStage({ from: account1 });          

            // add 0.00167 * 100 * 0.3 = 0.0501ETH to buyout fund
            await this.crowdsale.buyTokens(account2, 0x0, { from: account2, value: 100 * preSalePrice });
            
            try {
                await this.crowdsale.buyout(20, { from: account2 });
            } catch (error) {
                const isRevert = error.message.search('Over limit') >= 0;                
                assert(
                   isRevert,
                  "Expected throw, got '" + error + "' instead",
                );                
            }

            await this.crowdsale.setSale1Stage({ from: account1 });

            // account2 can return 33% of 100 tokens on sale 1.  33 * 0.0025 = 0.0825
            // var available = await this.crowdsale.checkBuyoutLimit();
            // console.log(`available ${available}`);

            await this.crowdsale.buyout(33, { from: account2 });

            try {
                await this.crowdsale.buyout(20, { from: account2 });
            } catch (error) {
                const isRevert = error.message.search('Over limit') >= 0;                
                assert(
                   isRevert,
                  "Expected throw, got '" + error + "' instead",
                );                
            }

            await this.crowdsale.setSale2Stage({ from: account1 });

            // account2 can return 58% of 100 tokens on sale 2.  25 * 0.00333 = 0.08325
            await this.crowdsale.buyout(25, { from: account2 });

            try {
                await this.crowdsale.buyout(20, { from: account2 });
            } catch (error) {
                const isRevert = error.message.search('Over limit') >= 0;                
                assert(
                   isRevert,
                  "Expected throw, got '" + error + "' instead",
                );                
            }            

            const tokensCount =  await this.token.balanceOf(account2);  
            const tokensCountOwner =  await this.token.balanceOf(this.crowdsale.address);  
            
            assert.equal(tokensCount.valueOf(), 42);
            assert.equal(tokensCountOwner.valueOf(), tokensLimit - (6000 + 42));
        });
    });
});