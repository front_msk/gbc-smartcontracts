const WinWinCoinCrowdsale = artifacts.require('WinWinCoinCrowdsale');
const WinWinCoinToken = artifacts.require("WinWinCoinToken");
const WinWinCoinWhitelist = artifacts.require("WinWinCoinWhitelist");

contract('WinWinCoinCrowdsaleTest', (accounts) => {
    const account1 = accounts[0];  
    const account2 = accounts[1];
    const account3 = accounts[2];

    const tokensLimit = 200 * 1000 * 1000;    
    const price = web3.toWei("0.00333", "ether");  
    const airdropMin = 20;  

    context('airdrop', async () => {
        beforeEach(async () => {
            this.token = await WinWinCoinToken.new();
            this.whitelist = await WinWinCoinWhitelist.new();
            this.crowdsale = await WinWinCoinCrowdsale.new(account1, this.token.address, this.whitelist.address);
            await this.token.transferOwnership(this.crowdsale.address);            
            await this.whitelist.addAddressToWhitelist(account1);
        });
        
        it('should fail to buy with airdrop when airdrop disabled', async () => {
            try {
                await this.crowdsale.buyWithAirdrop(account1, { value: airdropMin * price });
            } catch (error) {
                const isRevert = error.message.search('Airdrop disabled') >= 0;                
                assert(
                   isRevert,
                  "Expected throw, got '" + error + "' instead",
                );                
            }
        });

        it('should fail to buy with airdrop when count of tokens is less when minimum', async () => {
            await this.crowdsale.enableAirdrop({ from: account1 });
            try {
                await this.crowdsale.buyWithAirdrop(account1, { value: 10 * price });
            } catch (error) {
                const isRevert = error.message.search('Not enougth tokens') >= 0;                
                assert(
                   isRevert,
                  "Expected throw, got '" + error + "' instead",
                );                
            }
        });

        it('should check airdrop disabled', async () => {
            await this.crowdsale.enableAirdrop({ from: account1 });
            await this.crowdsale.disableAirdrop({ from: account1 });
            try {
                await this.crowdsale.buyWithAirdrop(account1, { value: airdropMin * price });
            } catch (error) {
                const isRevert = error.message.search('Airdrop disabled') >= 0;                
                assert(
                   isRevert,
                  "Expected throw, got '" + error + "' instead",
                );                
            }
        });

        it('should buy with airdrop', async () => {
            await this.crowdsale.enableAirdrop({ from: account1 });
            await this.crowdsale.buyWithAirdrop(account1, { value: airdropMin * price });
            
            const tokensCount =  await this.token.balanceOf(account1);
            const tokensInAirdropFundCount =  await this.crowdsale.tokensForAirdrop();

            assert.equal(tokensCount.valueOf(), 120);
            assert.equal(tokensInAirdropFundCount.valueOf(), 14000000 - 120);
        });

        it('should check airdrop bonus failed while crowdsale is not finalized', async () => {
            await this.crowdsale.enableAirdrop({ from: account1 });            
            try {
                await this.crowdsale.sendAirdropBonus(account2, { from: account1 });
            } catch (error) {
                const isRevert = error.message.search('Crowdsale is not finalized') >= 0;                
                assert(
                   isRevert,
                  "Expected throw, got '" + error + "' instead",
                );                
            }
        });

        it('should check airdrop bonus', async () => {
            await this.crowdsale.enableAirdrop({ from: account1 });
            
            await this.crowdsale.setPresaleStage({ from: account1 });
            await this.crowdsale.setSale1Stage({ from: account1 });
            await this.crowdsale.setSale2Stage({ from: account1 });            
            await this.crowdsale.finalize({ from: account1 });
            
            await this.crowdsale.sendAirdropBonus(account2, { from: account1 });
            await this.crowdsale.sendAirdropBonus(account3, { from: account1 });
            
            const tokensCountAcc2 =  await this.token.balanceOf(account2);
            const tokensCountAcc3 =  await this.token.balanceOf(account3);

            const tokensInAirdropFundCount =  await this.crowdsale.tokensForAirdrop();

            assert.equal(tokensCountAcc2.valueOf(), 30);
            assert.equal(tokensCountAcc3.valueOf(), 30);
            assert.equal(tokensInAirdropFundCount.valueOf(), 14000000 - 60);
        });
    });
});