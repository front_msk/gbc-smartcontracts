const WinWinCoinCrowdsale = artifacts.require('WinWinCoinCrowdsale');
const WinWinCoinToken = artifacts.require("WinWinCoinToken");
const WinWinCoinWhitelist = artifacts.require("WinWinCoinWhitelist");

contract('WinWinCoinCrowdsaleTest', (accounts) => {
    const account1 = accounts[0];
    const account2 = accounts[1];
    const account3 = accounts[2];

    const tokens = 4000;
    const price = web3.toWei("0.00083", "ether");

    context('referrals', async () => {
        beforeEach(async () => {
            this.token = await WinWinCoinToken.new();
            this.whitelist = await WinWinCoinWhitelist.new();
            this.crowdsale = await WinWinCoinCrowdsale.new(account1, this.token.address, this.whitelist.address);
            await this.token.transferOwnership(this.crowdsale.address);            
            await this.whitelist.addAddressToWhitelist(account1);
            await this.whitelist.addAddressToWhitelist(account2);
        });
        
        it('should buy with referral', async () => {
            const getBeneficiaryExpectedTokens = (iter) => {
                return tokens + (tokens + tokens * 0.1) * iter;
            }

            const getReferalExpectedTokens = (iter) => {
                return (tokens * 0.1) * iter;
            }

            const check = async (iter) => {
                const tokensCount =  await this.token.balanceOf(account1);
                const referralTokensCount =  await this.token.balanceOf(account2);

                assert.equal(tokensCount.valueOf(), getBeneficiaryExpectedTokens(iter));
                assert.equal(referralTokensCount.valueOf(), getReferalExpectedTokens(iter));
            }

            let purchaseWithRef = 0;
            await this.crowdsale.buyTokens(account1, 0x0, { from: account1, value: tokens * price });

            const tokensCount =  await this.token.balanceOf(account1);
            assert.equal(tokensCount.valueOf(), tokens);
            
            // buy with referral code
            purchaseWithRef++;
            await this.crowdsale.buyTokens(account1, account2, { from: account1, value: tokens * price });
            await check(purchaseWithRef);            
            
            // buy without referral code
            purchaseWithRef++;
            await this.crowdsale.buyTokens(account1, 0x0, { from: account1, value: tokens * price });
            await check(purchaseWithRef);

            // buy with another code
            purchaseWithRef++;
            await this.crowdsale.buyTokens(account1, account3, { from: account1, value: tokens * price });
            await check(purchaseWithRef);
        });

        it('should pay to referral by eth', async () => {            
            const amountEth = 0.0334;

            await this.crowdsale.setPresaleStage({ from: account1 });
            await this.crowdsale.setReferralBonusType(true, { from: account2 });
            const referralBalanceBefore =  await web3.eth.getBalance(account2);            
            await this.crowdsale.buyTokens(account1, account2, { from: account1, value: web3.toWei(amountEth, "ether") });            
            const referralBalanceAfter =  await web3.eth.getBalance(account2);
            
            const tokensCount =  await this.token.balanceOf(account1);
            const tokensCount2 =  await this.token.balanceOf(account2);            
            
            assert.equal(tokensCount.valueOf(), 22);
            assert.equal(tokensCount2.valueOf(), 0);

            const delta = referralBalanceAfter.valueOf() - referralBalanceBefore.valueOf();
            const expectedDelta = web3.toWei(amountEth * 0.05, "ether");
            const diff = Math.abs(delta - expectedDelta);
            const err = diff / delta;            

            assert.isTrue(err < 0.001);
        });
    });
});