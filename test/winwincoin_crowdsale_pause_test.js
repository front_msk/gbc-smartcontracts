const WinWinCoinCrowdsale = artifacts.require('WinWinCoinCrowdsale');
const WinWinCoinToken = artifacts.require("WinWinCoinToken");
const WinWinCoinWhitelist = artifacts.require("WinWinCoinWhitelist");

contract('WinWinCoinTransferTokensTest', function (accounts) {
    const account1 = accounts[0];
    const account2 = accounts[1];    

    const privateSalePrice = web3.toWei("0.00083", "ether");
    const airdropPrice = web3.toWei("0.00333", "ether");
    
    beforeEach(async () => {
        this.token = await WinWinCoinToken.new();
        this.whitelist = await WinWinCoinWhitelist.new();
        this.crowdsale = await WinWinCoinCrowdsale.new(account1, this.token.address, this.whitelist.address);
        await this.token.transferOwnership(this.crowdsale.address);        
    });   

    it("should check token buying failed when paused", async () => {        
        await this.crowdsale.pause({ from: account1 });       
        
        try {                
            await this.crowdsale.buyTokens(account2, 0x0, { from: account2, value: 4000 * privateSalePrice });
        } catch (error) {                
            const isRevert = error.message.search('revert') >= 0;                
            assert(
                isRevert,
                "Expected throw, got '" + error + "' instead",
            );            
        }

        await this.crowdsale.unpause({ from: account1 });
        
        await this.crowdsale.buyTokens(account2, 0x0, { from: account2, value: 4000 * privateSalePrice });

        const tokens = await this.token.balanceOf(account2);
        assert.equal(tokens.valueOf(), 4000);
    });

    it("should check airdrop when paused", async () => {        
        await this.crowdsale.enableAirdrop({ from: account1 });
        await this.crowdsale.pause({ from: account1 });     
        
        try {                
            await this.crowdsale.buyWithAirdrop(account2, { value: 20 * airdropPrice });
        } catch (error) {                
            const isRevert = error.message.search('revert') >= 0;                
            assert(
                isRevert,
                "Expected throw, got '" + error + "' instead",
            );            
        }

        await this.crowdsale.unpause({ from: account1 });
        await this.crowdsale.buyWithAirdrop(account2, { value: 20 * airdropPrice });
        
        const tokens = await this.token.balanceOf(account2);
        assert.equal(tokens.valueOf(), 120);
    });

    it("should check buyout when paused", async () => {        
        await this.crowdsale.buyTokens(account2, 0x0, { from: account2, value: 4000 * privateSalePrice });

        await this.crowdsale.setPresaleStage({ from: account1 });

        await this.crowdsale.pause({ from: account1 });
        
        try {                
            await this.crowdsale.buyout(500, { from: account2 });
        } catch (error) {                
            const isRevert = error.message.search('revert') >= 0;                
            assert(
                isRevert,
                "Expected throw, got '" + error + "' instead",
            );            
        }

        await this.crowdsale.unpause({ from: account1 });
        await this.crowdsale.buyout(500, { from: account2 });
        
        const tokens = await this.token.balanceOf(account2);
        assert.equal(tokens.valueOf(), 3500);
    });
});