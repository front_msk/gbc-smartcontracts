const WinWinCoinCrowdsale = artifacts.require('WinWinCoinCrowdsale');
const WinWinCoinToken = artifacts.require("WinWinCoinToken");
const WinWinCoinWhitelist = artifacts.require("WinWinCoinWhitelist");

contract('WinWinCoinWalletTest', function (accounts) {
    const account1 = accounts[0];
    const account2 = accounts[1];
    const wallet = accounts[2];

    const privateSalePrice = web3.toWei("0.00083", "ether");
    const airdropPrice = web3.toWei("0.00333", "ether");

    const round = (val) => Math.round(val * 10**10) / 10**10;
    
    beforeEach(async () => {
        this.token = await WinWinCoinToken.new();
        this.whitelist = await WinWinCoinWhitelist.new();
        this.crowdsale = await WinWinCoinCrowdsale.new(wallet, this.token.address, this.whitelist.address);
        await this.token.transferOwnership(this.crowdsale.address);                   
    });
    it("should check 70% of purchase amount transfer to wallet", async () => {        
        const walletBalanceBefore =  await web3.eth.getBalance(wallet);
        
        await this.crowdsale.buyTokens(account1, 0x0, { from: account1, value: 5000 * privateSalePrice });

        const crowdsaleBalance = await web3.eth.getBalance(this.crowdsale.address);
        const walletBalanceAfter =  await web3.eth.getBalance(wallet);
        const diff = round(walletBalanceAfter.valueOf() - walletBalanceBefore.valueOf());

        assert.equal(crowdsaleBalance.valueOf(), 5000 * privateSalePrice * 0.3);
        const percent = diff / (5000 * privateSalePrice);

        assert.isTrue(percent > 0.699);
        assert.isTrue(percent < 0.701);
    });

    it("should check 70% of purchase with airdrop amount transfer to wallet", async () => {        
        const walletBalanceBefore =  await web3.eth.getBalance(wallet);
        
        await this.crowdsale.enableAirdrop({ from: account1 });
        await this.crowdsale.buyWithAirdrop(account1, { from: account1, value: 20 * airdropPrice });

        const crowdsaleBalance = await web3.eth.getBalance(this.crowdsale.address);
        const walletBalanceAfter =  await web3.eth.getBalance(wallet);

        const diff = round(walletBalanceAfter.valueOf() - walletBalanceBefore.valueOf());

        assert.equal(crowdsaleBalance.valueOf(), 20 * airdropPrice * 0.3);
        const percent = diff / (20 * airdropPrice);

        assert.isTrue(percent > 0.699);
        assert.isTrue(percent < 0.701);
    });

    it("should check withdraw", async () => {        
        await this.crowdsale.buyTokens(account1, 0x0, { from: account2, value: 10000 * privateSalePrice });

        const walletBalanceBefore =  await web3.fromWei(web3.eth.getBalance(wallet));
        const crowdsaleBalanceBefore = await web3.fromWei(web3.eth.getBalance(this.crowdsale.address));
        
        await this.crowdsale.withdraw(10**18, { from: account1 });

        const walletBalanceAfter =  await web3.fromWei(web3.eth.getBalance(wallet));
        const crowdsaleBalanceAfter = await web3.fromWei(web3.eth.getBalance(this.crowdsale.address));        

        const walletDiff = walletBalanceAfter.valueOf() - walletBalanceBefore.valueOf();
        const crowdsaleDiff =  round(crowdsaleBalanceBefore.valueOf() - crowdsaleBalanceAfter.valueOf());

        assert.equal(walletDiff.valueOf(), crowdsaleDiff.valueOf());
        assert.equal(walletDiff.valueOf(), 1);        
    });

    it('should check deposit', async () => {
        const crowdsaleBalanceBefore = await web3.fromWei(web3.eth.getBalance(this.crowdsale.address));
        await this.crowdsale.deposit({ from: account1, value: 10 ** 18 });
        const crowdsaleBalanceAfter = await web3.fromWei(web3.eth.getBalance(this.crowdsale.address));        
        
        const crowdsaleDiff =  round(crowdsaleBalanceAfter.valueOf() - crowdsaleBalanceBefore.valueOf());

        assert.equal(crowdsaleDiff, 1);
    });
});