const WinWinCoinCrowdsale = artifacts.require('WinWinCoinCrowdsale');
const WinWinCoinToken = artifacts.require("WinWinCoinToken");
const WinWinCoinWhitelist = artifacts.require("WinWinCoinWhitelist");

contract('WinWinCoinCrowdsaleTest', (accounts) => {
    const account1 = accounts[0];
    const account2 = accounts[1];

    const tokensLimit = 200 * 1000 * 1000;
    const privateSalePrice = web3.toWei("0.00083", "ether");
    const preSalePrice = web3.toWei("0.00167", "ether");
    const sale1Price = web3.toWei("0.00250", "ether");
    const sale2Price = web3.toWei("0.00333", "ether");

    const privateSaleMinCount = 4000;
    const preSaleMinCount = 20;
    const sale1SaleMinCount = 15;
    const sale2SaleMinCount = 10;

    context('limits', async () => {
        beforeEach(async () => {
            this.token = await WinWinCoinToken.new();
            this.whitelist = await WinWinCoinWhitelist.new();
            this.crowdsale = await WinWinCoinCrowdsale.new(account1, this.token.address, this.whitelist.address);
            await this.token.freeze();
            await this.token.transferOwnership(this.crowdsale.address);            
            await this.whitelist.addAddressToWhitelist(account1);            
        });

        it("should check supply", async () => {
            const maxTokens = await this.crowdsale.maxTokens();

            assert.equal(maxTokens.valueOf(), tokensLimit);            
        });
               

        it('should tokens for sale limit', async () => {
            const countToBuy = 40000000; // with bonus 48M
            const received = 48000000;

            await this.crowdsale.buyTokens(accounts[0], 0x0, { from: accounts[0], value: countToBuy * privateSalePrice });

            const tokensCount =  await this.token.balanceOf(accounts[0]);
            const ownerTokensCount =  await this.token.balanceOf(this.crowdsale.address);
            
            assert.equal(tokensCount.valueOf(), received);
            assert.equal(ownerTokensCount.valueOf(), tokensLimit - received);

            try {
                const countToBuy2 = 20000000; // with bonus 24M
                await this.crowdsale.buyTokens(accounts[0], 0x0, { from: accounts[0], value: countToBuy2 * privateSalePrice });
            } catch(error) {                
                const isRevert = error.message.search('Not enougth tokens') >= 0;                
                assert(
                   isRevert,
                  "Expected throw, got '" + error + "' instead",
                );                
            }            
        });

        it('should check finalization', async () => {            
            await this.crowdsale.buyTokens(accounts[0], 0x0, { from: accounts[0], value: privateSaleMinCount * privateSalePrice });

            const tokensCount =  await this.token.balanceOf(accounts[0]);
            const saleFundTokensCount =  await this.crowdsale.tokensForSale();

            await this.crowdsale.setPresaleStage();
            await this.crowdsale.setSale1Stage();
            await this.crowdsale.setSale2Stage();
            await this.crowdsale.finalize();

            const saleFundTokensFinalizedCount =  await this.crowdsale.tokensForSale();
            const jackpotFundTokensFinalizedCount =  await this.crowdsale.tokensForJackpot();

            const tokenIsUnfreezed = !(await this.token.isFreezed());
            const tokenIsUnpaused = !(await this.token.paused());

            assert.isTrue(tokenIsUnfreezed);
            assert.isTrue(tokenIsUnpaused);
            
            assert.equal(tokensCount.valueOf(), privateSaleMinCount);
            assert.equal(saleFundTokensCount.valueOf(), 60 * 1000 * 1000 - privateSaleMinCount);
            assert.equal(saleFundTokensFinalizedCount.valueOf(), 0);
            assert.equal(jackpotFundTokensFinalizedCount.valueOf(), (80 + 60) * 1000 * 1000 - privateSaleMinCount);

            try {                
                await this.crowdsale.buyTokens(accounts[0], 0x0, { from: accounts[0], value: sale2SaleMinCount * sale2Price });
            } catch(error) {                
                const isRevert = error.message.search('Crowdsale is finalized') >= 0;                
                assert(
                   isRevert,
                  "Expected throw, got '" + error + "' instead",
                );                
            }            
        });

        it('should check min token amounts for buying in different stages', async () => {
            let total = 0;

            /* Private Sale */
            try {
                await this.crowdsale.buyTokens(accounts[0], 0x0, { from: accounts[0], value: 1 * privateSalePrice });
            } catch(error) {                
                const isRevert = error.message.search('Less that minimum amount') >= 0;                
                assert(
                   isRevert,
                  "Expected throw, got '" + error + "' instead",
                );  
            }
            
            await this.crowdsale.buyTokens(accounts[0], 0x0, { from: accounts[0], value: privateSaleMinCount * privateSalePrice });
            const afterPrivateSaleTokensCount =  await this.token.balanceOf(accounts[0]);
            const afterPrivateSaleOwnerTokensCount =  await this.token.balanceOf(this.crowdsale.address);

            total += privateSaleMinCount;
            assert.equal(afterPrivateSaleTokensCount.valueOf(), total);
            assert.equal(afterPrivateSaleOwnerTokensCount.valueOf(), tokensLimit - total);           

            /* Presale Sale */
            await this.crowdsale.setPresaleStage({ from: account1 });
            try {
                await this.crowdsale.buyTokens(accounts[0], 0x0, { from: accounts[0], value: 1 * preSalePrice });
            } catch(error) {
                const isRevert = error.message.search('Less that minimum amount') >= 0;                
                assert(
                   isRevert,
                  "Expected throw, got '" + error + "' instead",
                );  
            }            
            
            await this.crowdsale.buyTokens(accounts[0], 0x0, { from: accounts[0], value: preSaleMinCount * preSalePrice });
            const afterPresaleTokensCount =  await this.token.balanceOf(accounts[0]);
            const afterPresaleOwnerTokensCount =  await this.token.balanceOf(this.crowdsale.address);

            total += preSaleMinCount;

            assert.equal(afterPresaleTokensCount.valueOf(), total);
            assert.equal(afterPresaleOwnerTokensCount.valueOf(), tokensLimit - total);

            /* Sale1 */
            await this.crowdsale.setSale1Stage({ from: account1 });
            try {
                await this.crowdsale.buyTokens(accounts[0], 0x0, { from: accounts[0], value: 1 * sale1Price });
            } catch(error) {
                const isRevert = error.message.search('Less that minimum amount') >= 0;                
                assert(
                   isRevert,
                  "Expected throw, got '" + error + "' instead",
                );
            }            
            
            await this.crowdsale.buyTokens(accounts[0], 0x0, { from: accounts[0], value: sale1SaleMinCount * sale1Price });
            const afterSale1TokensCount =  await this.token.balanceOf(accounts[0]);
            const afterSale1OwnerTokensCount =  await this.token.balanceOf(this.crowdsale.address);

            total += sale1SaleMinCount;

            assert.equal(afterSale1TokensCount.valueOf(), total);
            assert.equal(afterSale1OwnerTokensCount.valueOf(), tokensLimit - total);

            /* Sale 2 */

            await this.crowdsale.setSale2Stage({ from: account1 });
            try {
                await this.crowdsale.buyTokens(accounts[0], 0x0, { from: accounts[0], value: 1 * sale2Price });
            } catch(error) {
                const isRevert = error.message.search('Less that minimum amount') >= 0;                
                assert(
                   isRevert,
                  "Expected throw, got '" + error + "' instead",
                ); 
            }            
            
            await this.crowdsale.buyTokens(accounts[0], 0x0, { from: accounts[0], value: sale2SaleMinCount * sale2Price });
            const afterSale2TokensCount =  await this.token.balanceOf(accounts[0]);
            const afterSale2OwnerTokensCount =  await this.token.balanceOf(this.crowdsale.address);

            total += sale2SaleMinCount;

            assert.equal(afterSale2TokensCount.valueOf(), total);
            assert.equal(afterSale2OwnerTokensCount.valueOf(), tokensLimit - total);
        });
    });
});