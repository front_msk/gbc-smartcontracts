const WinWinCoinCrowdsale = artifacts.require('WinWinCoinCrowdsale');
const WinWinCoinToken = artifacts.require("WinWinCoinToken");
const WinWinCoinWhitelist = artifacts.require("WinWinCoinWhitelist");

contract('WinWinCoinTransferTokensTest', function (accounts) {
    const account1 = accounts[0];
    const account2 = accounts[1];
    const account3 = accounts[2];

    const privateSalePrice = web3.toWei("0.00083", "ether");
    
    const check = async (amount, fund, fundAmount, fundAmountFx) => {            
        const fundBalanceBefore =  await fundAmountFx();
        const csTokensBefore =  await this.token.balanceOf(this.crowdsale.address);

        await this.crowdsale.transferTokensFromFund(account2, amount, fund, { from: account1 });

        const fundBalanceAfter =  await fundAmountFx();
        const csTokensAfter =  await this.token.balanceOf(this.crowdsale.address);

        const accTokens =  await this.token.balanceOf(account2);       
        
        const diff = csTokensBefore.valueOf() - csTokensAfter.valueOf();
        const diff2 = fundBalanceBefore.valueOf() - fundBalanceAfter.valueOf();
        
        assert.equal(diff, amount);
        assert.equal(diff2, amount);
        assert.equal(fundBalanceAfter.valueOf(), fundAmount);
        assert.equal(accTokens, amount);
    }
    
    beforeEach(async () => {
        this.token = await WinWinCoinToken.new();
        this.whitelist = await WinWinCoinWhitelist.new();
        this.crowdsale = await WinWinCoinCrowdsale.new(account1, this.token.address, this.whitelist.address);
        await this.token.transferOwnership(this.crowdsale.address);
        
        // await this.crowdsale.setPresaleStage({ from: account1 });
        // await this.crowdsale.setSale1Stage({ from: account1 });
        // await this.crowdsale.setSale2Stage({ from: account1 });
        // await this.crowdsale.finalize({ from: account1 });
    });
    it("should check transfer from advisers fund", async () => {        
        check(50, 0, 5000000 - 50, this.crowdsale.tokensForAdvisors);
    });
    it("should check transfer from airdrop fund", async () => { 
        check(50, 1, 14000000 - 50, this.crowdsale.tokensForAirdrop);
    });
    it("should check transfer from gamblers fund", async () => {
        check(50, 2, 26000000 - 50, this.crowdsale.tokensForGamblers);
    });    
    it("should check transfer from sale fund", async () => {        
        check(50, 4, 60000000 - 50, this.crowdsale.tokensForSale);
    });
    it("should check transfer from team fund", async () => {        
        check(50, 5, 15000000 - 50, this.crowdsale.tokensForTeam);
    });

    it("should check transfer from one user to another failed", async () => {        
        await this.crowdsale.buyTokens(account2, 0x0, { from: account2, value: 4000 * privateSalePrice });                
        
        try {                
            await this.token.transferFromTo(account2, account3, 1000, { from: account1 });
        } catch (error) {                
            const isRevert = error.message.search('revert') >= 0;                
            assert(
                isRevert,
                "Expected throw, got '" + error + "' instead",
            );
            return;
        }
    });

    it("should check transfer from one user to another by token owner", async () => {        
        await this.crowdsale.buyTokens(account2, 0x0, { from: account2, value: 4000 * privateSalePrice });        
        await this.crowdsale.transferToken(account1, { from: account1 });        
        await this.token.transferFromTo(account2, account3, 1000, { from: account1 });
        
        const acc2Tokens =  await this.token.balanceOf(account2);
        const acc3Tokens =  await this.token.balanceOf(account3);
        
        assert.equal(acc2Tokens.valueOf(), 3000);
        assert.equal(acc3Tokens.valueOf(), 1000);
    });
});