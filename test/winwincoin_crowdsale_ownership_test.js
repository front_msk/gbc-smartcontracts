const WinWinCoinCrowdsale = artifacts.require('WinWinCoinCrowdsale');
const WinWinCoinToken = artifacts.require("WinWinCoinToken");
const WinWinCoinWhitelist = artifacts.require("WinWinCoinWhitelist");

contract('WinWinCoinCrowdsaleTest ownership', (accounts) => {
    const account1 = accounts[0];    
    const account2 = accounts[1];     

    context('ownership', async () => {
        beforeEach(async () => {
            this.token = await WinWinCoinToken.new();
            this.whitelist = await WinWinCoinWhitelist.new();
            this.crowdsale = await WinWinCoinCrowdsale.new(account1, this.token.address, this.whitelist.address);
            await this.token.transferOwnership(this.crowdsale.address);            
        });
        
        it('should transfer crowdsale', async () => {
            const oldCSOwner = await this.crowdsale.owner();            

            try{
                await this.crowdsale.transferOwnership(account2, { from: account2 });
            } catch(error) {
                const isRevert = error.message.search('VM Exception while processing transaction: revert') >= 0;                
                assert(
                   isRevert,
                  "Expected throw, got '" + error + "' instead",
                );                
            }

            await this.crowdsale.transferOwnership(account2);

            const newCSOwner = await this.crowdsale.owner();            

            assert.equal(oldCSOwner, account1);            
            assert.equal(newCSOwner, account2);            
        });

        it('should transfer token ownership', async () => {            
            const oldTokenOwner = await this.token.owner();

            try{
                await this.crowdsale.transferToken(account2, { from: account2 });
            } catch(error) {
                const isRevert = error.message.search('VM Exception while processing transaction: revert') >= 0;                
                assert(
                   isRevert,
                  "Expected throw, got '" + error + "' instead",
                );                
            }

            await this.crowdsale.transferToken(account2);
            
            const newTokenOwner = await this.token.owner();                  

            assert.equal(oldTokenOwner, this.crowdsale.address);            
            assert.equal(newTokenOwner, account2);
        });        
    });
});