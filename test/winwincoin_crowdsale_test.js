const WinWinCoinCrowdsale = artifacts.require('WinWinCoinCrowdsale');
const WinWinCoinToken = artifacts.require("WinWinCoinToken");
const WinWinCoinWhitelist = artifacts.require("WinWinCoinWhitelist");

contract('WinWinCoinCrowdsaleTest', (accounts) => {
    const account1 = accounts[0];
    const account2 = accounts[1];

    const tokensLimit = 200 * 1000 * 1000;
    const privateSalePrice = web3.toWei("0.00083", "ether");
    const preSalePrice = web3.toWei("0.00167", "ether");
    const sale1Price = web3.toWei("0.00250", "ether");
    const sale2Price = web3.toWei("0.00333", "ether");

    const privateSaleMinCount = 4000;
    const preSaleMinCount = 20;
    const sale1SaleMinCount = 15;
    const sale2SaleMinCount = 10;

    context('with token', async () => {
        beforeEach(async () => {
            this.token = await WinWinCoinToken.new();
            this.whitelist = await WinWinCoinWhitelist.new();
            this.crowdsale = await WinWinCoinCrowdsale.new(account1, this.token.address, this.whitelist.address);
            await this.token.transferOwnership(this.crowdsale.address);            
        });        

        it('should check rate', async () => {
            const rate = await this.crowdsale.getPrice();

            assert.equal(rate.valueOf(), privateSalePrice);
        });

        it('should check whitelist revert when buy tokens', async () => {            
            await this.whitelist.enableWhitelist();

            try {                
                await this.crowdsale.buyTokens(accounts[0], 0x0, { from: accounts[0], value: 3 * privateSalePrice });
            } catch (error) {                
                const isRevert = error.message.search('VM Exception while processing transaction: revert') >= 0;                
                assert(
                   isRevert,
                  "Expected throw, got '" + error + "' instead",
                );
                return;
            }
            assert.fail('Expected throw not received');
        });

        it('should check whitelist', async () => {
            const notInWhitelistWhenDisabled = await this.whitelist.whitelist(accounts[0]);
            await this.whitelist.enableWhitelist();
            const notInWhitelist = await this.whitelist.whitelist(accounts[0]);
            
            await this.whitelist.addAddressToWhitelist(accounts[0]);
            const isInWhitelist = await this.whitelist.whitelist(accounts[0]); 
            
            assert.isTrue(notInWhitelistWhenDisabled);
            assert.isTrue(!notInWhitelist);
            assert.isTrue(isInWhitelist);
        });

        it('should buy tokens', async () => {            
            await this.whitelist.addAddressToWhitelist(accounts[0]);            

            const initialCount =  await this.token.balanceOf(accounts[0]);
            assert.equal(initialCount.valueOf(), 0);

            const countToBuy = privateSaleMinCount;

            await this.crowdsale.buyTokens(accounts[0], 0x0, { from: accounts[0], value: privateSaleMinCount * privateSalePrice });

            const tokensCount =  await this.token.balanceOf(accounts[0]);
            const ownerTokensCount =  await this.token.balanceOf(this.crowdsale.address);
            
            assert.equal(tokensCount.valueOf(), countToBuy);
            assert.equal(ownerTokensCount.valueOf(), tokensLimit - countToBuy);
        });        
        
        it('should buy tokens by transfer', async () => {
            await this.whitelist.addAddressToWhitelist(accounts[1]);

            const initialCount =  await this.token.balanceOf(accounts[1]);
            assert.equal(initialCount.valueOf(), 0);            

            await web3.eth.sendTransaction({ to: this.crowdsale.address, from: accounts[1], value: web3.toWei("4", "ether"), gas: 200000 });

            const tokensCount =  await this.token.balanceOf(accounts[1]);
            const ownerTokensCount =  await this.token.balanceOf(this.crowdsale.address);
            
            assert.equal(tokensCount.valueOf(), Math.floor(4 / 0.00083));
            assert.equal(ownerTokensCount.valueOf(), tokensLimit - Math.floor(4 / 0.00083));
        });

        it('should check stages switching', async () => {
            const privateSaleStage = await this.crowdsale.getStage();
            assert.equal(privateSaleStage.valueOf(), 0);

            try {
                await this.crowdsale.setPresaleStage({ from: account2 });
            } catch (error) {
                const isRevert = error.message.search('VM Exception while processing transaction: revert') >= 0;                
                assert(
                   isRevert,
                  "Expected throw, got '" + error + "' instead",
                );                
            }

            await this.crowdsale.setPresaleStage({ from: account1 });
            const preSaleStage = await this.crowdsale.getStage();
            assert.equal(preSaleStage.valueOf(), 1);

            await this.crowdsale.setSale1Stage({ from: account1 });
            const sale1SaleStage = await this.crowdsale.getStage();
            assert.equal(sale1SaleStage.valueOf(), 2);

            await this.crowdsale.setSale2Stage({ from: account1 });
            const sale2SaleStage = await this.crowdsale.getStage();
            assert.equal(sale2SaleStage.valueOf(), 3);            
        }); 

        it('should check prices', async () => {
            const privateSaleStagePrice = await this.crowdsale.getPrice();
            assert.equal(privateSaleStagePrice.valueOf(), privateSalePrice);

            await this.crowdsale.setPresaleStage({ from: account1 });
            const preSaleStagePrice = await this.crowdsale.getPrice();
            assert.equal(preSaleStagePrice.valueOf(), preSalePrice);

            await this.crowdsale.setSale1Stage({ from: account1 });
            const sale1StagePrice = await this.crowdsale.getPrice();
            assert.equal(sale1StagePrice.valueOf(), sale1Price);

            await this.crowdsale.setSale2Stage({ from: account1 });
            const sale2StagePrice = await this.crowdsale.getPrice();
            assert.equal(sale2StagePrice.valueOf(), sale2Price);            
        });        
    });
});