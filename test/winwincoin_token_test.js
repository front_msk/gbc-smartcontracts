const WinWinCoinToken = artifacts.require("WinWinCoinToken");

contract('WinWinCoinTokenTest', function (accounts) {
    const account1 = accounts[0];
    const account2 = accounts[1];
    
    beforeEach(async () => {
        this.token = await WinWinCoinToken.new();
    });
    it("should check token name and symbol", async () => {            
        const name = await this.token.name();
        const symbol = await this.token.symbol();

        assert.equal(name, 'WinWinCoinToken');
        assert.equal(symbol, 'WWC');
    });
    it("should check token supply", async () => {            
        const totalSupply = await this.token.totalSupply();

        assert.equal(totalSupply.valueOf(), 200 * 1000 * 1000);
    });    
});
