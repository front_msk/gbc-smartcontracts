const WinWinCoinCrowdsale = artifacts.require('WinWinCoinCrowdsale');
const WinWinCoinToken = artifacts.require("WinWinCoinToken");
const WinWinCoinWhitelist = artifacts.require("WinWinCoinWhitelist");

contract('WinWinCoinCrowdsaleTest', (accounts) => {
    const account1 = accounts[0];
    const account2 = accounts[1];
    //const zeroAddress = web3.utils.toAddress('0x0000000000000000000000000000000000000000');

    const tokensLimit = 200 * 1000 * 1000;
    const privateSalePrice = web3.toWei("0.00083", "ether");
    const preSalePrice = web3.toWei("0.00167", "ether");
    const sale1Price = web3.toWei("0.00250", "ether");
    const sale2Price = web3.toWei("0.00333", "ether");    

    context('with bonus', async () => {
        beforeEach(async () => {
            this.token = await WinWinCoinToken.new();
            this.whitelist = await WinWinCoinWhitelist.new();
            this.crowdsale = await WinWinCoinCrowdsale.new(account1, this.token.address, this.whitelist.address);
            await this.token.transferOwnership(this.crowdsale.address);            
            await this.whitelist.addAddressToWhitelist(account1);
        });

        const check = async (price, count, result) => {            
            await this.crowdsale.buyTokens(account1, 0x0, { from: account1, value: count * price });
            const tokensCount =  await this.token.balanceOf(account1);
            assert.equal(tokensCount.valueOf(), result);
        }

        it('should buy tokens with bonus 10 stage 0', async () => {
            await check(privateSalePrice, 80000, 88000);
        });
        it('should buy tokens with bonus 15 stage 0', async () => {            
            await check(privateSalePrice, 200000, 230000);
        });
        it('should buy tokens with bonus 20 stage 0', async () => {
            await check(privateSalePrice, 400000, 480000);
        });
        it('should buy tokens with bonus 10 stage 1', async () => {
            await this.crowdsale.setPresaleStage({ from: account1 });
            await check(preSalePrice, 2000, 2200);
        });
        it('should buy tokens with bonus 15 stage 1', async () => {            
            await this.crowdsale.setPresaleStage({ from: account1 });
            await check(preSalePrice, 10000, 11500);
        });
        it('should buy tokens with bonus 20 stage 1', async () => {            
            await this.crowdsale.setPresaleStage({ from: account1 });
            await check(preSalePrice, 20000, 24000);
        });
        it('should buy tokens with bonus 10 stage 2', async () => {
            await this.crowdsale.setPresaleStage({ from: account1 });
            await this.crowdsale.setSale1Stage({ from: account1 });
            check(sale1Price, 1350, 1485);
        });
        it('should buy tokens with bonus 15 stage 2', async () => {
            await this.crowdsale.setPresaleStage({ from: account1 });
            await this.crowdsale.setSale1Stage({ from: account1 });
            check(sale1Price, 6667, 7667);
        });
        it('should buy tokens with bonus 20 stage 2', async () => {
            await this.crowdsale.setPresaleStage({ from: account1 });
            await this.crowdsale.setSale1Stage({ from: account1 });
            check(sale1Price, 13350, 16020);
        });
        it('should buy tokens with bonus 10 stage 3', async () => {
            await this.crowdsale.setPresaleStage({ from: account1 });
            await this.crowdsale.setSale1Stage({ from: account1 });
            await this.crowdsale.setSale2Stage({ from: account1 });
            check(sale2Price, 1000, 1100);
        });
        it('should buy tokens with bonus 15 stage 3', async () => {
            await this.crowdsale.setPresaleStage({ from: account1 });
            await this.crowdsale.setSale1Stage({ from: account1 });
            await this.crowdsale.setSale2Stage({ from: account1 });
            check(sale2Price, 5000, 5750);
        });
        it('should buy tokens with bonus 20 stage 3', async () => {
            await this.crowdsale.setPresaleStage({ from: account1 });
            await this.crowdsale.setSale1Stage({ from: account1 });
            await this.crowdsale.setSale2Stage({ from: account1 });
            check(sale2Price, 10000, 12000);
        });
    });
});