const WinWinCoinToken = artifacts.require("WinWinCoinToken");

contract('WinWinCoinTokenFreezeTest', function (accounts) {
    const account1 = accounts[0];
    const account2 = accounts[1];
    
    beforeEach(async () => {
        this.token = await WinWinCoinToken.new();        
    });
    it("should check token freeze", async () => {        
        await this.token.freeze({ from: account1 });

        const isFreezed = await this.token.isFreezed();        
        assert.isTrue(isFreezed);

        await this.token.transfer(account2, 1000, { from: account1 });
        const balance = await this.token.balanceOf(account2);

        assert.equal(balance.valueOf(), 1000);

        try {                
            await this.token.transfer(account1, 500, { from: account2});
        } catch (error) {                
            const isRevert = error.message.search('revert Token freezed or paused') >= 0;                
            assert(
                isRevert,
                "Expected throw, got '" + error + "' instead",
            );
            return;
        }
    });

    it("should check token unfreeze", async () => {        
        await this.token.freeze({ from: account1 });
        const isFreezed = await this.token.isFreezed();        
        assert.isTrue(isFreezed);

        await this.token.transfer(account2, 1000, { from: account1 });

        await this.token.unfreeze({ from: account1 });
        const isUnfreezed = !(await this.token.isFreezed());
        assert.isTrue(isUnfreezed);

        await this.token.transfer(account1, 500, { from: account2});
        const balance = await this.token.balanceOf(account2);
        assert.equal(balance.valueOf(), 500);
    });
});